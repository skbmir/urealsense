#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/distortion_models.h>
#include <librealsense2/rs.hpp>
#include "readers.h"

using namespace urealsense;

int main(int argc, char** argv)
{
    // init node
    ros::init(argc, argv, "rs_depth_node");

    // create node handle
    ros::NodeHandle nh("~");

    // variables to store parameters
    std::string serial_num;
    std::string frame_id;
    int frame_rate;
    int frame_height;
    int frame_width;
    int decimation_rate;
    float min_depth_threshold;
    float max_depth_threshold;
    bool use_decimation_filter;
    bool use_threshold_filter;
    bool use_temporal_filter;
    bool use_spatial_filter;

    // get parameters
    nh.param<std::string>("serial_num", serial_num, "");
    nh.param<std::string>("frame_id", frame_id, "depth_camera");
    nh.param<int>("frame_rate", frame_rate, 30);
    nh.param<int>("frame_height", frame_height, 480);
    nh.param<int>("frame_width", frame_width, 640);
    nh.param<int>("decimation", decimation_rate, 2);
    nh.param<float>("min_depth_threshold", min_depth_threshold, 0.1);
    nh.param<float>("max_depth_threshold", max_depth_threshold, 8.0);
    nh.param<bool>("use_temporal_filter", use_temporal_filter, false);
    nh.param<bool>("use_spatial_filter", use_spatial_filter, false);
    nh.param<bool>("use_threshold_filter", use_threshold_filter, false);
    nh.param<bool>("use_decimation_filter", use_decimation_filter, false);

    // create librealsense context
    rs2::context context;
    // list realsense devices
    rs2::device_list devices = context.query_devices();

    // alignment
    rs2::align align_to_color(RS2_STREAM_COLOR);

    // filters
    rs2::decimation_filter decimation;
    rs2::spatial_filter spatial_filter;
    rs2::temporal_filter temporal_filter;
    rs2::threshold_filter threshold;

    // set filter options
    decimation.set_option(RS2_OPTION_FILTER_MAGNITUDE, decimation_rate);
    threshold.set_option(RS2_OPTION_MIN_DISTANCE, min_depth_threshold);
    threshold.set_option(RS2_OPTION_MAX_DISTANCE, max_depth_threshold);

    // if no realsense devices found
    if (devices.size() == 0)
    {
        ROS_ERROR("No RealSense devices found\n");
        nh.shutdown();
        return EXIT_FAILURE;
    }

    ROS_INFO("Found devices:\n");

    for (rs2::device device : devices)
    {
        // print found devices
        ROS_INFO("name: %s, sn: %s.", device.get_info(RS2_CAMERA_INFO_NAME), device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
    }

    // create capture pipeline
    rs2::pipeline camera_pipeline(context);
    // create capture configuration
    rs2::config config;
    config.enable_device(serial_num); // enable selected realsense device
    ROS_INFO("Enabled device: %s.", serial_num.c_str());

    // setup color stream
    config.enable_stream(RS2_STREAM_COLOR, frame_width, frame_height, RS2_FORMAT_BGR8, frame_rate);
    // create publisher
    ros::Publisher color_publisher = nh.advertise<sensor_msgs::Image>("color", 1);
    ros::Publisher color_intrinsics_publisher = nh.advertise<sensor_msgs::CameraInfo>("color/camera_info", 1);
    ROS_INFO("Color frame stream enabled.");

    // setup depth stream
    config.enable_stream(RS2_STREAM_DEPTH, frame_width, frame_height, RS2_FORMAT_Z16, frame_rate);
    // create publisher
    ros::Publisher depth_publisher = nh.advertise<sensor_msgs::Image>("depth", 1);
    ros::Publisher depth_intrinsics_publisher = nh.advertise<sensor_msgs::CameraInfo>("depth/camera_info", 1);
    ROS_INFO("Depth frame stream enabled.");

    // setup output image message
    sensor_msgs::Image color_img;
    color_img.header.frame_id = frame_id;
    color_img.height = frame_height;
    color_img.width = frame_width;
    color_img.encoding = sensor_msgs::image_encodings::BGR8;
    // setup color stream intrinsics message header
    sensor_msgs::CameraInfo color_info;
    color_info.header.frame_id = frame_id;

    // setup output depth message
    sensor_msgs::Image depth_img;
    depth_img.header.frame_id = frame_id;
    depth_img.height = frame_height;
    depth_img.width = frame_width;
    depth_img.encoding = sensor_msgs::image_encodings::TYPE_16UC1; // Z16
    // setup depth stream intrinsics message header
    sensor_msgs::CameraInfo depth_info;
    depth_info.header.frame_id = frame_id;

    DepthReader reader = DepthReader(frame_id,
                                     frame_height,
                                     frame_width,
                                     decimation_rate,
                                     min_depth_threshold,
                                     max_depth_threshold,
                                     use_decimation_filter,
                                     use_threshold_filter,
                                     use_temporal_filter,
                                     use_spatial_filter,
                                     &depth_publisher,
                                     &color_publisher,
                                     &depth_intrinsics_publisher,
                                     &color_intrinsics_publisher);

    camera_pipeline.start(config, reader); // start frame capture
    ROS_INFO("Started capturing frames.");

    ros::Rate rate(10);
    while (ros::ok())
    {
        ros::spinOnce();

        rate.sleep();
    }

    config.disable_all_streams();
    camera_pipeline.stop();

    return EXIT_SUCCESS;
}