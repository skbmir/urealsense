#include "RealTrackingNodelet.h"

namespace urealsense
{
    void RealTrackingNodelet::onInit()
    {
        nh = getPrivateNodeHandle();

        pose_publisher = nh.advertise<nav_msgs::Odometry>("odometry", 1);
        tf_broadcaster = tf::TransformBroadcaster();

        nh.param<std::string>("serial_num", serial_num, "");
        nh.param<std::string>("static_frame_id", static_frame_id, "realsense_odom");
        nh.param<std::string>("sensor_frame_id", sensor_frame_id, "tracking_camera");

        config.enable_device(serial_num);
        ROS_INFO("Enabled device: %s.", serial_num.c_str());
        // enable pose stream in 6DOF format
        config.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);

        pipeline = rs2::pipeline(context);

        reader = PoseReader(static_frame_id,
                            sensor_frame_id,
                            &pose_publisher,
                            &tf_broadcaster);

        pipeline.start(config, reader);
    }

    RealTrackingNodelet::~RealTrackingNodelet()
    {
        config.disable_all_streams();
        pipeline.stop();
    }
}

PLUGINLIB_EXPORT_CLASS(urealsense::RealTrackingNodelet, nodelet::Nodelet);