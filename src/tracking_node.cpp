#include <string>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <ros/console.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/transform_broadcaster.h>
#include <librealsense2/rs.hpp>
#include "readers.h"

using namespace urealsense;

int main(int argc, char** argv)
{
    // init node
    ros::init(argc, argv, "rs_tracking_node");

    // create node handle
    ros::NodeHandle nh("~");

    // variables to store parameters
    std::string serial_num;
    std::string static_frame_id;
    std::string sensor_frame_id;

    // get node parameters
    nh.param<std::string>("serial_num", serial_num, "");
    nh.param<std::string>("static_frame_id", static_frame_id, "realsense_odom");
    nh.param<std::string>("sensor_frame_id", sensor_frame_id, "tracking_camera");

    // create librealsense context
    rs2::context context;
    // list realsense devices
    rs2::device_list devices = context.query_devices();

    // if no realsense devices found
    if (devices.size() == 0)
    {
        ROS_ERROR("No RealSense devices found\n");
        nh.shutdown();
        return EXIT_FAILURE;
    }

    ROS_INFO("Found devices:\n");

    // for each device in found devices
    for (rs2::device device : devices)
    {
        //print found devices
        ROS_INFO("name: %s, sn: %s.", device.get_info(RS2_CAMERA_INFO_NAME), device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
    }

    // Publishers for pose and transform
    ros::Publisher pose_publisher = nh.advertise<nav_msgs::Odometry>("odometry", 1);
    tf::TransformBroadcaster tf_broadcaster;

    // odometry message
    nav_msgs::Odometry odom_msg;
    odom_msg.header.frame_id = static_frame_id;
    odom_msg.child_frame_id = sensor_frame_id;

    // transform message
    geometry_msgs::TransformStamped tf_msg;
    tf_msg.header.frame_id = static_frame_id;
    tf_msg.child_frame_id = sensor_frame_id;

    // stream configuration storage
    rs2::config config;
    // enable selected device or any available
    config.enable_device(serial_num);
    ROS_INFO("Enabled device: %s.", serial_num.c_str());
    // enable pose stream in 6DOF format
    config.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);

    rs2::pipeline pipeline(context);

    PoseReader reader = PoseReader(static_frame_id,
                                   sensor_frame_id,
                                   &pose_publisher,
                                   &tf_broadcaster);

    pipeline.start(config, reader);

    ros::Rate rate(10);
    while (ros::ok())
    {
        ros::spinOnce();

        rate.sleep();
    }

    config.disable_all_streams();
    pipeline.stop();

    return EXIT_SUCCESS;
}