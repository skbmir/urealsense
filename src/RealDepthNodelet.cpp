#include "RealDepthNodelet.h"

namespace urealsense
{
    void RealDepthNodelet::onInit()
    {
        nh = getPrivateNodeHandle();

        // get parameters
        nh.param<std::string>("serial_num", serial_num, "");
        nh.param<std::string>("frame_id", frame_id, "depth_camera");
        nh.param<int>("frame_rate", frame_rate, 30);
        nh.param<int>("frame_height", frame_height, 480);
        nh.param<int>("frame_width", frame_width, 640);
        nh.param<int>("decimation", decimation_rate, 2);
        nh.param<float>("min_depth_threshold", min_depth_threshold, 0.1);
        nh.param<float>("max_depth_threshold", max_depth_threshold, 8.0);
        nh.param<bool>("use_temporal_filter", use_temporal_filter, false);
        nh.param<bool>("use_spatial_filter", use_spatial_filter, false);
        nh.param<bool>("use_threshold_filter", use_threshold_filter, false);
        nh.param<bool>("use_decimation_filter", use_decimation_filter, false);

        color_publisher = nh.advertise<sensor_msgs::Image>("color", 1);
        color_info_publisher = nh.advertise<sensor_msgs::CameraInfo>("color/camera_info", 1);
        depth_publisher = nh.advertise<sensor_msgs::Image>("depth", 1);
        depth_info_publisher = nh.advertise<sensor_msgs::CameraInfo>("depth/camera_info", 1);

        config.enable_device(serial_num); // enable selected realsense device
        NODELET_INFO("Enabled device: %s.", serial_num.c_str());

        // setup color stream
        config.enable_stream(RS2_STREAM_COLOR, frame_width, frame_height, RS2_FORMAT_BGR8, frame_rate);
        NODELET_INFO("Color frame stream enabled.");

        // setup depth stream
        config.enable_stream(RS2_STREAM_DEPTH, frame_width, frame_height, RS2_FORMAT_Z16, frame_rate);
        NODELET_INFO("Depth frame stream enabled.");

        pipeline = rs2::pipeline(context);

        reader = DepthReader(frame_id,
                                frame_height,
                                frame_width,
                                decimation_rate,
                                min_depth_threshold,
                                max_depth_threshold,
                                use_decimation_filter,
                                use_threshold_filter,
                                use_temporal_filter,
                                use_spatial_filter,
                                &depth_publisher,
                                &color_publisher,
                                &depth_info_publisher,
                                &color_info_publisher);

        pipeline.start(config, reader);
        NODELET_INFO("Started capturing frames.");
    }

    RealDepthNodelet::~RealDepthNodelet()
    {
        config.disable_all_streams();
        pipeline.stop();
    }
}

PLUGINLIB_EXPORT_CLASS(urealsense::RealDepthNodelet, nodelet::Nodelet);