#include "readers.h"

namespace urealsense
{
    PoseReader::PoseReader() {}

    PoseReader::PoseReader(std::string static_frame,
                           std::string sensor_frame,
                           ros::Publisher* pose_publisher,
                           tf::TransformBroadcaster* tf_broadcaster) :
                           pose_publisher_p(pose_publisher),
                           tf_broadcaster_p(tf_broadcaster)
    {
        odom_msg.header.frame_id = tf_msg.header.frame_id = static_frame;
        odom_msg.child_frame_id = tf_msg.child_frame_id = sensor_frame;
    }

    void PoseReader::operator()(const rs2::frame& frame)
    {
        auto timestamp = ros::Time::now();
        odom_msg.header.stamp = timestamp;
        tf_msg.header.stamp = timestamp;

        if (rs2::pose_frame pose_frame = frame.as<rs2::pose_frame>())
        {
            rs2_pose pose_data = pose_frame.get_pose_data();

            // see realsense T265 documentation
            // axes of T265 are not aligned with axes in ROS
            // so x_ros == -z_t265; y_ros = -x_t265; z_ros = y_t265
            // fill up position relative to start point
            odom_msg.pose.pose.position.x = tf_msg.transform.translation.x = pose_data.translation.z * (-1.0f);
            odom_msg.pose.pose.position.y = tf_msg.transform.translation.y = pose_data.translation.x * (-1.0f);
            odom_msg.pose.pose.position.z = tf_msg.transform.translation.z = pose_data.translation.y;
            // fill up orientation relative to start orientation
            odom_msg.pose.pose.orientation.w = tf_msg.transform.rotation.w = pose_data.rotation.w;
            odom_msg.pose.pose.orientation.x = tf_msg.transform.rotation.x = pose_data.rotation.z * (-1.0f);
            odom_msg.pose.pose.orientation.y = tf_msg.transform.rotation.y = pose_data.rotation.x * (-1.0f);
            odom_msg.pose.pose.orientation.z = tf_msg.transform.rotation.z = pose_data.rotation.y;
            // fill angular and linear speed data
            odom_msg.twist.twist.linear.x = pose_data.velocity.z * (-1.0f);
            odom_msg.twist.twist.linear.y = pose_data.velocity.x * (-1.0f);
            odom_msg.twist.twist.linear.z = pose_data.velocity.y;
            odom_msg.twist.twist.angular.x = pose_data.angular_velocity.z * (-1.0f);
            odom_msg.twist.twist.angular.y = pose_data.angular_velocity.x * (-1.0f);
            odom_msg.twist.twist.angular.z = pose_data.angular_velocity.y;
            
            // publish message and transform
            pose_publisher_p->publish(odom_msg);
            tf_broadcaster_p->sendTransform(tf_msg);
        }
    }


    DepthReader::DepthReader() : align_to_color(RS2_STREAM_COLOR) {}

    DepthReader::DepthReader(std::string frame,
                             int height,
                             int width,
                             int decimation,
                             float min_depth_thld,
                             float max_depth_thld,
                             bool use_decimation,
                             bool use_threshold,
                             bool use_temporal,
                             bool use_spatial,
                             ros::Publisher* depth_pub,
                             ros::Publisher* color_pub,
                             ros::Publisher* depth_info_pub,
                             ros::Publisher* color_info_pub) :
                             frame_id(frame),
                             frame_height(height),
                             frame_width(width),
                             decimation_rate(decimation),
                             min_depth_threshold(min_depth_thld),
                             max_depth_threshold(max_depth_thld),
                             use_decimation_filter(use_decimation),
                             use_threshold_filter(use_threshold),
                             use_temporal_filter(use_temporal),
                             use_spatial_filter(use_spatial),
                             depth_publisher_p(depth_pub),
                             color_publisher_p(color_pub),
                             depth_info_publisher_p(depth_info_pub),
                             color_info_publisher_p(color_info_pub),
                             align_to_color(RS2_STREAM_COLOR)
    {
        color_img.header.frame_id = depth_img.header.frame_id = color_info.header.frame_id = depth_info.header.frame_id = frame;
        color_img.height = depth_img.height = height;
        color_img.width = depth_img.width = width;
        color_img.encoding = sensor_msgs::image_encodings::BGR8;
        depth_img.encoding = sensor_msgs::image_encodings::TYPE_16UC1; // Z16
        // set filter options
        decimation_filter.set_option(RS2_OPTION_FILTER_MAGNITUDE, decimation_rate);
        threshold_filter.set_option(RS2_OPTION_MIN_DISTANCE, min_depth_threshold);
        threshold_filter.set_option(RS2_OPTION_MAX_DISTANCE, max_depth_threshold);
    }

    void DepthReader::operator()(const rs2::frame& frame)
    {
        if (rs2::frameset frames = frame.as<rs2::frameset>())
        {
            ros::Time timestamp = ros::Time::now();
            color_img.header.stamp = timestamp;
            color_info.header.stamp = timestamp;
            depth_img.header.stamp = timestamp;
            depth_info.header.stamp = timestamp;

            frames = align_to_color.process(frames);

            rs2::video_frame color = frames.get_color_frame();
            if (use_decimation_filter)
            {
                color = decimation_filter.process(color);
                color_img.height = color.get_height();
                color_img.width = color.get_width();
            }
            //copy color frame data from rs2 frame to image message
            auto color_data_ptr = (uint8_t*)color.get_data();
            color_img.data = std::vector<uint8_t>(color_data_ptr, color_data_ptr+color.get_height() * color.get_width() * color.get_bytes_per_pixel());
            // set image row length in bytes
            color_img.step = color.get_stride_in_bytes();

            // get intrinsics for color stream
            auto color_stream = color.get_profile().as<rs2::video_stream_profile>();
            rs2_intrinsics color_intrinsics = color_stream.get_intrinsics();
            // set distortion model and coefficients
            color_info.distortion_model = sensor_msgs::distortion_models::PLUMB_BOB;
            color_info.D = std::vector<double>({0.0, 0.0, 0.0, 0.0, 0.0});
            // set intrinsic camera matrix
            color_info.K[0] = color_intrinsics.fx;
            color_info.K[2] = color_intrinsics.ppx;
            color_info.K[4] = color_intrinsics.fy;
            color_info.K[5] = color_intrinsics.ppy;
            color_info.K[8] = 1.0;
            color_info.K[1] = color_info.K[3] = color_info.K[6] = color_info.K[7] = 0.0;
            //set rectification matrix
            color_info.R = boost::array<double, 9UL>({1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0});
            // set projection camera matrix
            color_info.P[0] = color_intrinsics.fx;
            color_info.P[2] = color_intrinsics.ppx;
            color_info.P[5] = color_intrinsics.fy;
            color_info.P[6] = color_intrinsics.ppy;
            color_info.P[10] = 1.0;
            color_info.P[1] = color_info.P[3] = color_info.P[4] = color_info.P[7] = color_info.P[8] = color_info.P[9] = color_info.P[11] = 0.0;
            // set binning refers
            color_info.binning_x = color_info.binning_y = 0;
            // set ROI
            color_info.roi.x_offset = color_info.roi.y_offset = color_info.roi.height = color_info.roi.width = 0;
            color_info.roi.do_rectify = false;
            // set frame height and width used for calibration
            color_info.height = color_intrinsics.height;
            color_info.width = color_intrinsics.width;

            rs2::depth_frame depth = frames.get_depth_frame();
            if (use_decimation_filter)
            {
                depth = decimation_filter.process(depth);
                depth_img.height = depth.get_height();
                depth_img.width = depth.get_width();
            }
            if (use_threshold_filter)
                depth = threshold_filter.process(depth);
            if (use_spatial_filter)
                depth = spatial_filter.process(depth);
            if (use_temporal_filter)
                depth = temporal_filter.process(depth);
            // copy depth frame data from rs2 depth frame to image message
            auto depth_data_ptr = (uint8_t*)depth.get_data();
            depth_img.data = std::vector<uint8_t>(depth_data_ptr, depth_data_ptr + depth.get_height() * depth.get_width() * depth.get_bytes_per_pixel());
            // set depth image row length in bytes
            depth_img.step = depth.get_stride_in_bytes();

            // get intrinsics for color stream
            auto depth_stream = depth.get_profile().as<rs2::video_stream_profile>();
            rs2_intrinsics depth_intrinsics = depth_stream.get_intrinsics();
            // set distortion model and coefficients
            depth_info.distortion_model = sensor_msgs::distortion_models::PLUMB_BOB;
            depth_info.D = std::vector<double>({0.0, 0.0, 0.0, 0.0, 0.0});
            // set intrinsic camera matrix
            depth_info.K[0] = depth_intrinsics.fx;
            depth_info.K[2] = depth_intrinsics.ppx;
            depth_info.K[4] = depth_intrinsics.fy;
            depth_info.K[5] = depth_intrinsics.ppy;
            depth_info.K[8] = 1.0;
            depth_info.K[1] = depth_info.K[3] = depth_info.K[6] = depth_info.K[7] = 0.0;
            // set rectification matrix
            depth_info.R = boost::array<double, 9UL>({1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0});
            // set projection camera matrix
            depth_info.P[0] = depth_intrinsics.fx;
            depth_info.P[2] = depth_intrinsics.ppx;
            depth_info.P[5] = depth_intrinsics.fy;
            depth_info.P[6] = depth_intrinsics.ppy;
            depth_info.P[10] = 1.0;
            depth_info.P[1] = depth_info.P[3] = depth_info.P[4] = depth_info.P[7] = depth_info.P[8] = depth_info.P[9] = depth_info.P[11] = 0.0;
            // set binning refers
            depth_info.binning_x = depth_info.binning_y = 0;
            // set ROI
            depth_info.roi.x_offset = depth_info.roi.y_offset = depth_info.roi.height = depth_info.roi.width = 0;
            depth_info.roi.do_rectify = false;
            // set frame height and width used for calibration
            depth_info.height = depth_intrinsics.height;
            depth_info.width = depth_intrinsics.width;

            // publish images
            color_info_publisher_p->publish(color_info);
            color_publisher_p->publish(color_img);
            depth_info_publisher_p->publish(depth_info);
            depth_publisher_p->publish(depth_img);
        }
    }
}