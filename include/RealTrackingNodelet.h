#ifndef REALTRACKINGNODELET_H
#define REALTRACKINGNODELET_H

#include <cstdio>
#include <cstdlib>
#include <string>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <ros/console.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <librealsense2/rs.hpp>
#include "readers.h"

namespace urealsense
{

    class RealTrackingNodelet : public nodelet::Nodelet
    {
        private:

        ros::NodeHandle nh;
        ros::Publisher pose_publisher;
        tf::TransformBroadcaster tf_broadcaster;

        rs2::config config;
        rs2::pipeline pipeline;
        rs2::context context;

        PoseReader reader;

        std::string serial_num;
        std::string static_frame_id;
        std::string sensor_frame_id;

        virtual void onInit() override;

        public:
        ~RealTrackingNodelet();
    };
}

#endif // REALTRACKINGNODELET_H