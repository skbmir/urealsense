#ifndef REALDEPTHNODELET_H
#define REALDEPTHNODELET_H

#include <string>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <ros/console.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/distortion_models.h>
#include <librealsense2/rs.hpp>
#include "readers.h"

namespace urealsense
{

    class RealDepthNodelet : public nodelet::Nodelet
    {
        private:

        ros::NodeHandle nh;
        ros::Publisher depth_publisher;
        ros::Publisher color_publisher;
        ros::Publisher depth_info_publisher;
        ros::Publisher color_info_publisher;

        std::string serial_num;
        std::string frame_id;
        int frame_rate;
        int frame_height;
        int frame_width;
        int decimation_rate;
        float min_depth_threshold;
        float max_depth_threshold;
        bool use_decimation_filter;
        bool use_threshold_filter;
        bool use_temporal_filter;
        bool use_spatial_filter;

        rs2::config config;
        rs2::pipeline pipeline;
        rs2::context context;

        DepthReader reader;

        virtual void onInit() override;

        public:
        ~RealDepthNodelet();
    };
}

#endif