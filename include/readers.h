#ifndef READERS_H
#define READERS_H

#include <cstdio>
#include <cstdlib>
#include <string>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/distortion_models.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <librealsense2/rs.hpp>

namespace urealsense
{
    class PoseReader
    {
        private:

        nav_msgs::Odometry odom_msg;
        geometry_msgs::TransformStamped tf_msg;
        ros::Publisher* pose_publisher_p;
        tf::TransformBroadcaster* tf_broadcaster_p;

        public:

        PoseReader();

        PoseReader(std::string static_frame,
                   std::string sensor_frame,
                   ros::Publisher* pose_publisher,
                   tf::TransformBroadcaster* tf_broadcaster);

        void operator()(const rs2::frame& frame);
    };

    class DepthReader
    {
        private:

        std::string frame_id;
        int frame_height;
        int frame_width;
        int decimation_rate;
        float min_depth_threshold;
        float max_depth_threshold;
        bool use_decimation_filter;
        bool use_threshold_filter;
        bool use_temporal_filter;
        bool use_spatial_filter;

        // filters
        rs2::decimation_filter decimation_filter;
        rs2::spatial_filter spatial_filter;
        rs2::temporal_filter temporal_filter;
        rs2::threshold_filter threshold_filter;
        // alignment
        rs2::align align_to_color;

        sensor_msgs::Image depth_img;
        sensor_msgs::Image color_img;
        sensor_msgs::CameraInfo depth_info;
        sensor_msgs::CameraInfo color_info;
        ros::Publisher* depth_publisher_p;
        ros::Publisher* color_publisher_p;
        ros::Publisher* depth_info_publisher_p;
        ros::Publisher* color_info_publisher_p;

        public:

        DepthReader();

        DepthReader(std::string frame,
                    int height,
                    int width,
                    int decimation,
                    float min_depth_thld,
                    float max_depth_thld,
                    bool use_decimation,
                    bool use_threshold,
                    bool use_temporal,
                    bool use_spatial,
                    ros::Publisher* depth_pub,
                    ros::Publisher* color_pub,
                    ros::Publisher* depth_info_pub,
                    ros::Publisher* color_info_pub);

        void operator()(const rs2::frame& frame);
    };
}

#endif // READERS_H